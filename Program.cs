﻿using System;

namespace calc
{
    class Program
    {
        static void Main(string[] args)
        {
            double a;
            double b;
            double result;
            char o;

            Console.WriteLine("Что будем делать? ( + - / * )");
            o = Convert.ToChar(Console.ReadLine());
            Console.WriteLine("Введите первое число:");
            a = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine("Введите второе число:");
            b = Convert.ToDouble(Console.ReadLine());
            if (o == '+')
            {
                result = a + b;
                Console.WriteLine("Cумма " + a + " и " + b + " равна " + result);
            }
            else if (o == '-')
            {
                result = a - b;
                Console.WriteLine("Разность " + a + " и " + b + " равна " + result);
            }
            else if (o == '*')
            {
                result = a * b;
                Console.WriteLine("Умножение " + a + " и " + b + " равно " + result);
            }
            else if (o == '/')
            {
                result = a / b;
                Console.WriteLine("Деление " + a + " на " + b + " равно " + result);
            }
            else
            {
                Console.WriteLine("Но-но-но");
            }
        }
    }
}
